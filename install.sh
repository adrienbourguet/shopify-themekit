#!/bin/bash
# Stop on error
set -e
# Install mandatory programs and utilities
export DEBIAN_FRONTEND=noninteractive
apt-get --yes --quiet=2 update
apt-get --yes --quiet=2 install \
	apt-transport-https \
	ca-certificates \
	curl \
	dnsutils \
	git \
	jq \
	procps \
	rsync \
	wget \
	unzip \
	zip
# Purge unnecessary files
rm -rf /var/lib/apt/lists/*

# Install Theme Kit
FLAVOUR=$(uname -sm | tr [A-Z] [a-z] | sed -e 's/ /-/g' -e 's/x86_64/amd64/g')
URL=$(curl -s https://shopify-themekit.s3.amazonaws.com/releases/latest.json | jq '.platforms[] | select(.name=="'"${FLAVOUR}"'") | .url' | sed -e 's/"//g')
curl -s -o /usr/bin/theme "${URL}"
chmod +x /usr/bin/theme
